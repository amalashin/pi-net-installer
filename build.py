import os
import shutil
from debian import debfile

RASPBIAN = 'raspbian'
RPI_DEBIAN = 'debian'

REPO_DIR = './repo'  # local repository dir
RASPBIAN_REPO_DIR = REPO_DIR + '/' + RASPBIAN
RPI_DEBIAN_REPO_DIR = REPO_DIR + '/' + RPI_DEBIAN
PACKAGES_DIR = REPO_DIR + '/packages'
BLDROOT_DIR = REPO_DIR + '/buildroot'
BOOTFS_DIR = REPO_DIR + '/bootfs'
ROOTFS_DIR = REPO_DIR + '/rootfs'
INSTALLER_DIR = BOOTFS_DIR + '/installer'


def extract_packages():
    shutil.rmtree(BLDROOT_DIR, ignore_errors=True)
    os.makedirs(BLDROOT_DIR)
    packages = [p for p in os.listdir(PACKAGES_DIR) if os.path.splitext(p)[1] == '.deb']
    print('Got {} packages'.format(len(packages)))
    for package in packages:
        print('Extracting package: {}'.format(package))
        deb = debfile.DebFile(PACKAGES_DIR + '/' + package)
        deb.data.tgz().extractall(BLDROOT_DIR, numeric_owner=True)
    print('Done!')


def find_dependencies(kmod, dep_file, modules):
    with open(dep_file) as f:
        for line in f:
            kmod_info = line.split(':')
            if kmod_info[0] == kmod:
                deps = kmod_info[1].strip().split(' ')
                if not len(deps):
                    modules += [find_dependencies(dep, dep_file, modules) for dep in deps]
                else:
                    return modules
                #kmods = [find_dependencies(dep, dep_file, modules) for dep in deps]
                #return kmods
    return None


def create_cpio():
    shutil.rmtree(ROOTFS_DIR, ignore_errors=True)

    # create installer (rootfs) directories
    with open('./installer_dirs.list') as f:
        for line in f:
            line = line.strip()
            if line:
                os.makedirs(ROOTFS_DIR + line)

    # copy kernel modules info to rootfs
    for root, dirs, files in os.walk(BLDROOT_DIR + '/lib/modules/'):
        if 'modules.order' in files:
            kernel_dir = ROOTFS_DIR + root[len(BLDROOT_DIR):]  # extract only the kernel dir name
            os.makedirs(kernel_dir)
            shutil.copy2(root + '/modules.order', kernel_dir)
            shutil.copy2(root + '/modules.builtin', kernel_dir)
            shutil.copy2(root + '/modules.dep', kernel_dir)

    kernel_dirs = next(os.walk(ROOTFS_DIR + '/lib/modules'))[1]
    modules = ['kernel/fs/btrfs/btrfs.ko', 'kernel/drivers/scsi/sg.ko']  # required kernel modules
    for kernel in kernel_dirs:
        kmods = modules
        for kmod in kmods:
            deps = find_dependencies(kmod, '{}/lib/modules/{}/modules.dep'.format(ROOTFS_DIR, kernel))


    # for each kernel
        # kmods = modules
        # load modules.dep
            # recursively get dependencies for each kmod in kmods
            # copy dependencies from BLDROOT_DIR/lib/modules/.../kernel to ROOTFS_DIR/lib/modules/.../kernel

def main():
    extract_packages()
    shutil.copytree(BLDROOT_DIR + '/boot', BOOTFS_DIR, True)
    create_cpio()


if __name__ == '__main__':
    main()
